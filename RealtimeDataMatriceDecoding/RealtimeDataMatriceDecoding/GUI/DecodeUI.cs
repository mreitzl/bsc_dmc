﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using RealtimeDataMatriceDecoding.GUI;
using RealtimeDataMatriceDecoding.core.boundaries;
using RealtimeDataMatriceDecoding.core.view;
using RealtimeDataMatriceDecoding.core.entities;
using AForge.Video.DirectShow;
using RealtimeDataMatriceDecoding.core.presenter;
using RealtimeDataMatriceDecoding.core.validation;
using RealtimeDataMatriceDecoding.core.interactor;
using RealtimeDataMatriceDecoding.core.request;
using AForge.Video;

namespace RealtimeDataMatriceDecoding
{
    public partial class DecodeUI : Form, IDecodeView
    {
       
        private IconButton currentBtn;
        private Panel leftBorderBtn;
        private Form currentChild;
        FileLoader fileloader = new FileLoader();
        Converter converter = new Converter();
        CameraDevice cameradevice = new CameraDevice();
        AdjustableRectangle rectangle = new AdjustableRectangle();

        //FileLoader fileloader; //= new FileLoader();
        //Converter converter; //= new Converter();
        //CameraDevice cameradevice;// = new CameraDevice();
        //AdjustableRectangle rectangle;// = new AdjustableRectangle();
        _Decoder decoder;// = new Decoder();
        DecodePresenter presenter;// = new DecodeDMCPresenter(this);
        ParameterValidator paraVal;// = new ParameterValidator();
        DecodeInteractor interactor;// = new DecodeDMCInteractor(presenter, paraVal, decoder);
        static DecodeRequest request;// = new DecodeDMCRequest();

        static Bitmap Frame;
        static Bitmap loadedImage;
        
       
        int framecounter;


        public DecodeUI()
        {
            InitializeComponent();

            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 60);
            panelMenu.Controls.Add(leftBorderBtn);

            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
            cameradevice.videoSRC = new VideoCaptureDevice();
        }

        private void DecodeUI_Load(object sender, EventArgs e)
        {

            

            if (cameradevice.videoSRC.IsRunning)
            {
                cameradevice.videoSRC.Stop();
            }

            cameradevice.videoSRC = new VideoCaptureDevice(cameradevice.captureDevice[Properties.Settings.Default.Setting_Device_Index].MonikerString);
            cameradevice.videoSRC.NewFrame += new NewFrameEventHandler(VideoSource_NewFrame);
            cameradevice.videoSRC.Start();
            //cameradevice.InitializeStream();
            

        }


        public void VideoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            //if (videoStream.Image != null)
            //{
            //    videoStream.Image.Dispose();
            //}

            presenter = new DecodePresenter(this);
            paraVal = new ParameterValidator();
            decoder = new _Decoder();
            interactor = new DecodeInteractor(presenter, paraVal, decoder);
            request = new DecodeRequest();
            request.imageBM = (Bitmap)videoStream.Image;

            if (fileloader.isloaded)
            {
                Frame = loadedImage;
                framecounter++;
            }
            else
            {
                Frame = (Bitmap)eventArgs.Frame.Clone();
                framecounter++;
                Console.WriteLine(Frame);

            }
            //if (rectangle.isDrawn)
            //{
            
            request.area = new Rectangle(rectangle.rectX, rectangle.rectY, rectangle.rectW, rectangle.rectH);
            lock (request)
            {
                using (Graphics gr = Graphics.FromImage(Frame))
                {
                    gr.DrawRectangle(rectangle.pen, request.area);
                }
            }
            


            //}

            lock (Frame)
                {
                    videoStream.Image = Frame;
                }
            

            //drawRect((Bitmap)videoStream.Image);
           

            //(Bitmap)eventArgs.Frame.Clone();//(Bitmap)videoStream.Image;//Properties.Resources.Chip_straight_lowerlight;
            //initDecoding(request.imageBM);
            if (framecounter > 5)
            {
                InteractorExe();
                framecounter = 0;
            }
            //Frame.Dispose();
            //videoStream.Image.Dispose();

        }
        
        private void InteractorExe()
        {
            

            lock (request)
            {
                request.imageBM = request.imageBM.Clone(request.area, request.imageBM.PixelFormat);
            }

            interactor.execute(request);
            //Bitmap debdecodeArea = request.imageBM.Clone(request.area, request.imageBM.PixelFormat);

                
            debugBox.Image = converter.ConvertToBlackWhite(request.imageBM);
        }
            //request.imageBM.Dispose();
            //return;
            //videoStream.Refresh();

        

        public void show(DecodeViewModel viewModel)
        {
            txtImageName.Invoke(new MethodInvoker(delegate () { txtImageName.Text = viewModel.imageName; }));
            txtRectangleDimension.Invoke(new MethodInvoker(delegate () { txtRectangleDimension.Text = viewModel.rectangleDimension; }));
            txtDmCode.Invoke(new MethodInvoker(delegate () { txtDmCode.Text = viewModel.dmCode; }));
        }

        public void showValidationError(string errorMessage)
        {
            errMsgBox.Invoke(new MethodInvoker(delegate () { errMsgBox.Text = errorMessage; }));
            //errMsgBox.Text = errorMessage;
        }

        private void videoStream_MouseDown(object sender, MouseEventArgs e)
        {
            base.OnMouseDown(e);
            
            if (e.Button == MouseButtons.Left)
            {
                Cursor = Cursors.Cross;
                rectangle.pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                rectangle.rectX = e.X;
                rectangle.rectY = e.Y;

            }
        }

        private void videoStream_MouseEnter(object sender, EventArgs e)
        {
            base.OnMouseEnter(e);
            Cursor = Cursors.Cross;
        }

        private void videoStream_MouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                rectangle.rectW = e.X - rectangle.rectX;
                rectangle.rectH = e.Y - rectangle.rectY;
            }
        }

        private void videoStream_MouseUp(object sender, MouseEventArgs e)
        {
            base.OnMouseUp(e);
            txtRectangleDimension.Text = "X: " + rectangle.rectX + "Y: " + rectangle.rectY + "W: " + rectangle.rectW + "H: " + rectangle.rectH;
            rectangle.isDrawn = true;
            //if (fileloader.isloaded)
            //{
            //    InteractorExe();
            //}
        }

        private void panelDesktop_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        //Paint on VideoStream (Picturebox)
        //private void videoStream_Paint(object sender, PaintEventArgs e)
        //{
        //    rectangle.pen.Color = Color.Red;

        //    //e.Graphics.DrawRectangle(rectangle.pen, new Rectangle(rectangle.rectX, rectangle.rectY, rectangle.rectW, rectangle.rectH));
        //    //videoStream.Refresh();
        //}

        private struct RGBColors
        {
            public static Color colorbtn_Engineering = Color.FromArgb(146, 130, 133);
            public static Color colorbtn_Ocean = Color.FromArgb(155, 195, 183);
            public static Color colorbtn_Lawn = Color.FromArgb(174, 192, 103);
            public static Color colorbtn_Berry = Color.FromArgb(171, 55, 122);
        }

        //Button Layout/Functionalities
        private void ActiveButton(object senderBtn, Color color)
        {
            if(senderBtn != null)
            {
                DisableButton();
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(146, 130, 133);
                currentBtn.ForeColor = color;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = color;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;
                //Left border button
                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0,currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
                //change child icon
                iconChildForm.IconChar = currentBtn.IconChar;
                iconChildForm.IconColor = color;
            }
        }

        private void DisableButton()
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(146, 130, 133);
                currentBtn.ForeColor = Color.Gainsboro;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.Gainsboro;
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText; 
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        //Child Form Behaviour (Settings)
        private void ChildFormOpen(Form childForm) {
            if(currentChild != null)
            {
                currentChild.Close();
            }
            currentChild = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelDesktop.Controls.Add(childForm);
            panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            lblHome.Text = childForm.Text;

        }

        //Button Session
        private void btnSession_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.colorbtn_Lawn);
            if (currentChild != null)
            {
                currentChild.Close();
            }
            //fix -> TODO check if area selection isnt session button
            if (btnSession.Text == "Start Session"&&currentBtn==btnSession)
            {
                lblHome.Text = "Active Session";
                btnSession.Text = "Stop Session";
            }
            else
            {
                btnSession.Text = "Start Session";
                lblHome.Text = "Session Stopped";
            }
            
        }

        //Button Load
        private void btnLoad_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.colorbtn_Lawn);
            if (currentChild != null)
            {
                currentChild.Close();
            }

            lblHome.Text = "Load";

            //cameradevice.StopStream(cameradevice.videoSRC);

            loadedImage = fileloader.LoadImage();
            if (loadedImage != null)
            {
                //if (cameradevice.videoSRC.IsRunning)
                //{
                //    cameradevice.videoSRC.Stop();
                //}
                request.imageBM = loadedImage;
                //videoStream.Image = loadedImage;
                fileloader.isloaded = true;
            }
            else
            {
                MessageBox.Show("No Image loaded");
                cameradevice.videoSRC.Start() ;            
            }
 
        }

        //Button Save
        private void btnSave_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.colorbtn_Lawn);
            if (currentChild != null)
            {
                currentChild.Close();
            }
            lblHome.Text = "Save";
        }

        //Button Save
        private void btnCapture_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.colorbtn_Lawn);
            if(currentChild != null){
                currentChild.Close();
            }
            
            lblHome.Text = "Capture";
        }
        //Button Settings
        private void btnSettings_Click(object sender, EventArgs e)
        {
            ActiveButton(sender, RGBColors.colorbtn_Lawn);
            ChildFormOpen(new SettingsUI(cameradevice, fileloader));
        }

        //Button Home (Icon)
        private void btnHome_Click(object sender, EventArgs e)
        {
            if (currentChild != null)
            {
                currentChild.Close();
            }
            Reset();
        }

        //Reset Function after clicking Icon
        private void Reset()
        {
            DisableButton();
            leftBorderBtn.Visible = false;
            iconChildForm.IconChar = IconChar.Home;
            iconChildForm.IconColor = RGBColors.colorbtn_Lawn;
            lblHome.Text = "Home";
            btnSession.Text = "Start Session";
        }
        //Drag Form
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

     
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int WMsg, int wParam, int lParam);

        
        private void panelBar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            Application.Exit();
        }

        private void btnMinimze_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void DecodeUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cameradevice.videoSRC.IsRunning)
            {
                cameradevice.videoSRC.Stop();
            }
            videoStream.Dispose();
            openFileDialog1.Dispose();
            Frame.Dispose();
            if (loadedImage != null)
            {
                loadedImage.Dispose();
            }
            
        }
    }
}
