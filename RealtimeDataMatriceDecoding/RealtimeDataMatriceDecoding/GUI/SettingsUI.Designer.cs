﻿namespace RealtimeDataMatriceDecoding.GUI
{
    partial class SettingsUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.deviceBox = new System.Windows.Forms.ComboBox();
            this.cnfbtnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // deviceBox
            // 
            this.deviceBox.FormattingEnabled = true;
            this.deviceBox.Location = new System.Drawing.Point(139, 39);
            this.deviceBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.deviceBox.Name = "deviceBox";
            this.deviceBox.Size = new System.Drawing.Size(122, 21);
            this.deviceBox.TabIndex = 0;
            this.deviceBox.SelectedIndexChanged += new System.EventHandler(this.deviceBox_SelectedIndexChanged);
            // 
            // cnfbtnStart
            // 
            this.cnfbtnStart.Location = new System.Drawing.Point(307, 39);
            this.cnfbtnStart.Name = "cnfbtnStart";
            this.cnfbtnStart.Size = new System.Drawing.Size(75, 23);
            this.cnfbtnStart.TabIndex = 1;
            this.cnfbtnStart.Text = "Start";
            this.cnfbtnStart.UseVisualStyleBackColor = true;
            this.cnfbtnStart.Click += new System.EventHandler(this.cnfbtnStart_Click);
            // 
            // SettingsUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(195)))), ((int)(((byte)(183)))));
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.cnfbtnStart);
            this.Controls.Add(this.deviceBox);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SettingsUI";
            this.Text = "SettingsUI";
            this.Load += new System.EventHandler(this.SettingsUI_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox deviceBox;
        private System.Windows.Forms.Button cnfbtnStart;
    }
}