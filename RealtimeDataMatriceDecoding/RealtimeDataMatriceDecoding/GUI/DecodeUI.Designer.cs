﻿namespace RealtimeDataMatriceDecoding
{
    partial class DecodeUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnSettings = new FontAwesome.Sharp.IconButton();
            this.btnCapture = new FontAwesome.Sharp.IconButton();
            this.btnSave = new FontAwesome.Sharp.IconButton();
            this.btnLoad = new FontAwesome.Sharp.IconButton();
            this.btnSession = new FontAwesome.Sharp.IconButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnHome = new System.Windows.Forms.PictureBox();
            this.panelBar = new System.Windows.Forms.Panel();
            this.btnMinimze = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblHome = new System.Windows.Forms.Label();
            this.iconChildForm = new FontAwesome.Sharp.IconPictureBox();
            this.panelDesktop = new System.Windows.Forms.Panel();
            this.debugBox = new System.Windows.Forms.PictureBox();
            this.errMsgBox = new System.Windows.Forms.TextBox();
            this.txtDmCode = new System.Windows.Forms.TextBox();
            this.txtRectangleDimension = new System.Windows.Forms.TextBox();
            this.txtImageName = new System.Windows.Forms.TextBox();
            this.videoStream = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnHome)).BeginInit();
            this.panelBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconChildForm)).BeginInit();
            this.panelDesktop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.debugBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoStream)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(130)))), ((int)(((byte)(133)))));
            this.panelMenu.Controls.Add(this.btnSettings);
            this.panelMenu.Controls.Add(this.btnCapture);
            this.panelMenu.Controls.Add(this.btnSave);
            this.panelMenu.Controls.Add(this.btnLoad);
            this.panelMenu.Controls.Add(this.btnSession);
            this.panelMenu.Controls.Add(this.panel2);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(147, 408);
            this.panelMenu.TabIndex = 0;
            // 
            // btnSettings
            // 
            this.btnSettings.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Nirmala UI", 9F);
            this.btnSettings.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnSettings.IconChar = FontAwesome.Sharp.IconChar.Cogs;
            this.btnSettings.IconColor = System.Drawing.Color.Gainsboro;
            this.btnSettings.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSettings.IconSize = 32;
            this.btnSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.Location = new System.Drawing.Point(0, 369);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(2);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Padding = new System.Windows.Forms.Padding(7, 0, 13, 0);
            this.btnSettings.Size = new System.Drawing.Size(147, 39);
            this.btnSettings.TabIndex = 5;
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnCapture
            // 
            this.btnCapture.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapture.FlatAppearance.BorderSize = 0;
            this.btnCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCapture.Font = new System.Drawing.Font("Nirmala UI", 9F);
            this.btnCapture.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCapture.IconChar = FontAwesome.Sharp.IconChar.Cube;
            this.btnCapture.IconColor = System.Drawing.Color.Gainsboro;
            this.btnCapture.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCapture.IconSize = 32;
            this.btnCapture.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCapture.Location = new System.Drawing.Point(0, 208);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(2);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Padding = new System.Windows.Forms.Padding(7, 0, 13, 0);
            this.btnCapture.Size = new System.Drawing.Size(147, 39);
            this.btnCapture.TabIndex = 4;
            this.btnCapture.Text = "Capture";
            this.btnCapture.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCapture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Nirmala UI", 9F);
            this.btnSave.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnSave.IconChar = FontAwesome.Sharp.IconChar.FileExport;
            this.btnSave.IconColor = System.Drawing.Color.Gainsboro;
            this.btnSave.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSave.IconSize = 32;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(0, 169);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(7, 0, 13, 0);
            this.btnSave.Size = new System.Drawing.Size(147, 39);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Font = new System.Drawing.Font("Nirmala UI", 9F);
            this.btnLoad.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLoad.IconChar = FontAwesome.Sharp.IconChar.FileUpload;
            this.btnLoad.IconColor = System.Drawing.Color.Gainsboro;
            this.btnLoad.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnLoad.IconSize = 32;
            this.btnLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoad.Location = new System.Drawing.Point(0, 130);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Padding = new System.Windows.Forms.Padding(7, 0, 13, 0);
            this.btnLoad.Size = new System.Drawing.Size(147, 39);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load";
            this.btnLoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSession
            // 
            this.btnSession.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSession.FlatAppearance.BorderSize = 0;
            this.btnSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSession.Font = new System.Drawing.Font("Nirmala UI", 9F);
            this.btnSession.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnSession.IconChar = FontAwesome.Sharp.IconChar.Play;
            this.btnSession.IconColor = System.Drawing.Color.Gainsboro;
            this.btnSession.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnSession.IconSize = 32;
            this.btnSession.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSession.Location = new System.Drawing.Point(0, 91);
            this.btnSession.Margin = new System.Windows.Forms.Padding(2);
            this.btnSession.Name = "btnSession";
            this.btnSession.Padding = new System.Windows.Forms.Padding(7, 0, 13, 0);
            this.btnSession.Size = new System.Drawing.Size(147, 39);
            this.btnSession.TabIndex = 1;
            this.btnSession.Text = "Start Session";
            this.btnSession.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSession.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSession.UseVisualStyleBackColor = true;
            this.btnSession.Click += new System.EventHandler(this.btnSession_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnHome);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(147, 91);
            this.panel2.TabIndex = 0;
            // 
            // btnHome
            // 
            this.btnHome.Image = global::RealtimeDataMatriceDecoding.Properties.Resources.IFX_LOGO_RGB;
            this.btnHome.Location = new System.Drawing.Point(8, 8);
            this.btnHome.Margin = new System.Windows.Forms.Padding(2);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(129, 71);
            this.btnHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnHome.TabIndex = 0;
            this.btnHome.TabStop = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panelBar
            // 
            this.panelBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(130)))), ((int)(((byte)(133)))));
            this.panelBar.Controls.Add(this.btnMinimze);
            this.panelBar.Controls.Add(this.btnClose);
            this.panelBar.Controls.Add(this.lblHome);
            this.panelBar.Controls.Add(this.iconChildForm);
            this.panelBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBar.Location = new System.Drawing.Point(147, 0);
            this.panelBar.Margin = new System.Windows.Forms.Padding(2);
            this.panelBar.Name = "panelBar";
            this.panelBar.Size = new System.Drawing.Size(638, 49);
            this.panelBar.TabIndex = 1;
            this.panelBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelBar_MouseDown);
            // 
            // btnMinimze
            // 
            this.btnMinimze.FlatAppearance.BorderSize = 0;
            this.btnMinimze.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimze.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnMinimze.Location = new System.Drawing.Point(584, 8);
            this.btnMinimze.Margin = new System.Windows.Forms.Padding(2);
            this.btnMinimze.Name = "btnMinimze";
            this.btnMinimze.Size = new System.Drawing.Size(21, 29);
            this.btnMinimze.TabIndex = 3;
            this.btnMinimze.Text = "_";
            this.btnMinimze.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMinimze.UseVisualStyleBackColor = true;
            this.btnMinimze.Click += new System.EventHandler(this.btnMinimze_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnClose.Location = new System.Drawing.Point(609, 8);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(21, 29);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblHome
            // 
            this.lblHome.AutoSize = true;
            this.lblHome.Font = new System.Drawing.Font("Nirmala UI", 12F);
            this.lblHome.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblHome.Location = new System.Drawing.Point(55, 16);
            this.lblHome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHome.Name = "lblHome";
            this.lblHome.Size = new System.Drawing.Size(52, 21);
            this.lblHome.TabIndex = 1;
            this.lblHome.Text = "Home";
            // 
            // iconChildForm
            // 
            this.iconChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(130)))), ((int)(((byte)(133)))));
            this.iconChildForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(192)))), ((int)(((byte)(103)))));
            this.iconChildForm.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconChildForm.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(192)))), ((int)(((byte)(103)))));
            this.iconChildForm.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconChildForm.IconSize = 37;
            this.iconChildForm.Location = new System.Drawing.Point(13, 8);
            this.iconChildForm.Margin = new System.Windows.Forms.Padding(2);
            this.iconChildForm.Name = "iconChildForm";
            this.iconChildForm.Size = new System.Drawing.Size(38, 37);
            this.iconChildForm.TabIndex = 0;
            this.iconChildForm.TabStop = false;
            // 
            // panelDesktop
            // 
            this.panelDesktop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(130)))), ((int)(((byte)(133)))));
            this.panelDesktop.Controls.Add(this.debugBox);
            this.panelDesktop.Controls.Add(this.errMsgBox);
            this.panelDesktop.Controls.Add(this.txtDmCode);
            this.panelDesktop.Controls.Add(this.txtRectangleDimension);
            this.panelDesktop.Controls.Add(this.txtImageName);
            this.panelDesktop.Controls.Add(this.videoStream);
            this.panelDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDesktop.Location = new System.Drawing.Point(147, 49);
            this.panelDesktop.Margin = new System.Windows.Forms.Padding(2);
            this.panelDesktop.Name = "panelDesktop";
            this.panelDesktop.Size = new System.Drawing.Size(638, 359);
            this.panelDesktop.TabIndex = 2;
            this.panelDesktop.MouseEnter += new System.EventHandler(this.panelDesktop_MouseEnter);
            // 
            // debugBox
            // 
            this.debugBox.Location = new System.Drawing.Point(511, 208);
            this.debugBox.Margin = new System.Windows.Forms.Padding(2);
            this.debugBox.Name = "debugBox";
            this.debugBox.Size = new System.Drawing.Size(120, 118);
            this.debugBox.TabIndex = 5;
            this.debugBox.TabStop = false;
            // 
            // errMsgBox
            // 
            this.errMsgBox.Location = new System.Drawing.Point(518, 102);
            this.errMsgBox.Margin = new System.Windows.Forms.Padding(2);
            this.errMsgBox.Multiline = true;
            this.errMsgBox.Name = "errMsgBox";
            this.errMsgBox.Size = new System.Drawing.Size(108, 98);
            this.errMsgBox.TabIndex = 4;
            // 
            // txtDmCode
            // 
            this.txtDmCode.Location = new System.Drawing.Point(518, 64);
            this.txtDmCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtDmCode.Name = "txtDmCode";
            this.txtDmCode.Size = new System.Drawing.Size(108, 20);
            this.txtDmCode.TabIndex = 3;
            // 
            // txtRectangleDimension
            // 
            this.txtRectangleDimension.Location = new System.Drawing.Point(518, 42);
            this.txtRectangleDimension.Margin = new System.Windows.Forms.Padding(2);
            this.txtRectangleDimension.Name = "txtRectangleDimension";
            this.txtRectangleDimension.Size = new System.Drawing.Size(108, 20);
            this.txtRectangleDimension.TabIndex = 2;
            // 
            // txtImageName
            // 
            this.txtImageName.Location = new System.Drawing.Point(518, 19);
            this.txtImageName.Margin = new System.Windows.Forms.Padding(2);
            this.txtImageName.Name = "txtImageName";
            this.txtImageName.Size = new System.Drawing.Size(108, 20);
            this.txtImageName.TabIndex = 1;
            // 
            // videoStream
            // 
            this.videoStream.Location = new System.Drawing.Point(31, 19);
            this.videoStream.Margin = new System.Windows.Forms.Padding(2);
            this.videoStream.Name = "videoStream";
            this.videoStream.Size = new System.Drawing.Size(468, 320);
            this.videoStream.TabIndex = 0;
            this.videoStream.TabStop = false;
            this.videoStream.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoStream_MouseDown);
            this.videoStream.MouseEnter += new System.EventHandler(this.videoStream_MouseEnter);
            this.videoStream.MouseMove += new System.Windows.Forms.MouseEventHandler(this.videoStream_MouseMove);
            this.videoStream.MouseUp += new System.Windows.Forms.MouseEventHandler(this.videoStream_MouseUp);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // DecodeUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 408);
            this.Controls.Add(this.panelDesktop);
            this.Controls.Add(this.panelBar);
            this.Controls.Add(this.panelMenu);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DecodeUI";
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.Cyan;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DecodeUI_FormClosing);
            this.Load += new System.EventHandler(this.DecodeUI_Load);
            this.panelMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnHome)).EndInit();
            this.panelBar.ResumeLayout(false);
            this.panelBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconChildForm)).EndInit();
            this.panelDesktop.ResumeLayout(false);
            this.panelDesktop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.debugBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoStream)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private FontAwesome.Sharp.IconButton btnSettings;
        private FontAwesome.Sharp.IconButton btnCapture;
        private FontAwesome.Sharp.IconButton btnSave;
        private FontAwesome.Sharp.IconButton btnLoad;
        private FontAwesome.Sharp.IconButton btnSession;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox btnHome;
        private System.Windows.Forms.Panel panelBar;
        private FontAwesome.Sharp.IconPictureBox iconChildForm;
        private System.Windows.Forms.Label lblHome;
        private System.Windows.Forms.Panel panelDesktop;
        private System.Windows.Forms.Button btnMinimze;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox videoStream;
        private System.Windows.Forms.TextBox txtDmCode;
        private System.Windows.Forms.TextBox txtRectangleDimension;
        private System.Windows.Forms.TextBox txtImageName;
        private System.Windows.Forms.TextBox errMsgBox;
        private System.Windows.Forms.PictureBox debugBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

