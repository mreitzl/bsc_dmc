﻿using AForge.Video;
using AForge.Video.DirectShow;
using RealtimeDataMatriceDecoding.core.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealtimeDataMatriceDecoding.GUI
{
    public partial class SettingsUI : Form
    {
        DecodeUI decodeui = new DecodeUI();
        CameraDevice cameradevice;// = new CameraDevice();
        FileLoader fileloader;



        public SettingsUI(CameraDevice cameradevice, FileLoader fileloader)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.cameradevice = cameradevice;
            this.fileloader = fileloader;
            
        }

        private void SettingsUI_Load(object sender, EventArgs e)
        {
            
            foreach (FilterInfo Device in cameradevice.captureDevice)
            {
                deviceBox.Items.Add(Device.Name);
            }
            deviceBox.SelectedIndex = 0;
            Properties.Settings.Default.Setting_Device_Index = deviceBox.SelectedIndex;
            Properties.Settings.Default.Save();
        }

        private void deviceBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Setting_Device = deviceBox.SelectedItem.ToString();
            Properties.Settings.Default.Setting_Device_Index = deviceBox.SelectedIndex;
            Properties.Settings.Default.Save();

            //cameradevice.videoSRC = new VideoCaptureDevice(cameradevice.captureDevice[Properties.Settings.Default.Setting_Device_Index].MonikerString);
            ////cameradevice.videoSRC.NewFrame += new NewFrameEventHandler(VideoSource_NewFrame);
            //cameradevice.videoSRC.Start();


            Console.WriteLine(deviceBox.SelectedItem.ToString());
            Console.WriteLine("Device: "+Properties.Settings.Default.Setting_Device.ToString());
            Console.WriteLine("Device_Index: "+Properties.Settings.Default.Setting_Device_Index.ToString());
        }

        private void cnfbtnStart_Click(object sender, EventArgs e)
        {
            fileloader.isloaded = false;
            cameradevice.videoSRC = new VideoCaptureDevice(cameradevice.captureDevice[Properties.Settings.Default.Setting_Device_Index].MonikerString);
            cameradevice.videoSRC.NewFrame += new NewFrameEventHandler(decodeui.VideoSource_NewFrame);
            cameradevice.videoSRC.Start();

        }
    }
}
