﻿using RealtimeDataMatriceDecoding.core.boundaries;
using RealtimeDataMatriceDecoding.core.response;
using RealtimeDataMatriceDecoding.core.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.presenter
{
    public class DecodePresenter : IDecodePresenter
    {
        private IDecodeView view;

        public DecodePresenter(IDecodeView view)
        {
            this.view = view;
        }
        public void present(DecodeResponse response)
        {
            DecodeViewModel viewModel = new DecodeViewModel();
            viewModel.imageName = response.imageName;
            viewModel.rectangleDimension = response.rectangleDimension;
            viewModel.dmCode = response.dmCode;
            view.show(viewModel);
        }

        public void showValidationError(string errMsg)
        {
            view.showValidationError(errMsg);
        }
    }
}
