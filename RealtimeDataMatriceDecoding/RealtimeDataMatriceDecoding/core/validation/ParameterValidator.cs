﻿using RealtimeDataMatriceDecoding.core.boundaries;
using RealtimeDataMatriceDecoding.core.request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.validation
{
    public class ParameterValidator : IValidator
    {
        public ValidateResult validate(IRequest irequest)
        {
            DecodeRequest request = (DecodeRequest)irequest;
            //ValidateResult validateResult;
            if (!(request.area != null && request.area.X > 0 && request.area.Y > 0 && request.area.Width > 0 && request.area.Height > 0 && request.imageBM != null))
            {
                return new ValidateResult("Rectangle out of Area or no Image");
            }
            return new ValidateResult();
        }
    }
}
