﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.validation
{
    public class ValidateResult
    {
        private bool _isValid;
        private string errMsg;

        public ValidateResult(string errMsg)
        {
            if (errMsg == null)
            {
                this._isValid = true;
            }
            else
            {
                this.errMsg = errMsg;
                this._isValid = false;

            }
        }

        public ValidateResult() : this(null)
        {
            this._isValid = true;
        }

        public bool isValid() { return _isValid; }
        public string getErrorMessage() { return errMsg; }
    }
}
