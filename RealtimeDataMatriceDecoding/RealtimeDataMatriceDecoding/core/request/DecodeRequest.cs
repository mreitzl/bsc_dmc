﻿using RealtimeDataMatriceDecoding.core.boundaries;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.request
{
    public class DecodeRequest : IRequest
    {
        public Bitmap imageBM;
        public Rectangle area;
    }
}
