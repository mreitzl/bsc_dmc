﻿using RealtimeDataMatriceDecoding.core.boundaries;
using RealtimeDataMatriceDecoding.core.entities;
using RealtimeDataMatriceDecoding.core.request;
using RealtimeDataMatriceDecoding.core.response;
using RealtimeDataMatriceDecoding.core.validation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;

namespace RealtimeDataMatriceDecoding.core.interactor
{
    public class DecodeInteractor
    {
        private IDecodePresenter presenter;
        private ParameterValidator validator;
        private _Decoder decoder;
        private Converter converter;
        bool recognizedFlag = false;

        public DecodeInteractor(IDecodePresenter presenter, ParameterValidator validator, _Decoder decoder)
        {
            this.presenter = presenter;
            this.validator = validator;
            this.decoder = decoder;

        }

        public void execute(DecodeRequest request)
        {

            DecodeResponse response = new DecodeResponse();
            ValidateResult validateResult = validator.validate(request);
            converter = new Converter();
            if (!validateResult.isValid())
            {
                presenter.showValidationError(validateResult.getErrorMessage());
                return;
            }

           

            Result result = decoder.reader.Decode(converter.ConvertToBlackWhite(request.imageBM));

            if (result != null )
            {
                
                Console.WriteLine("decode, give it a try");
                //MessageBox.Show("Success");
                response.imageName = "testImageName";
                response.rectangleDimension = "X:" + request.area.X + "Y: " + request.area.Y + "W: " + request.area.Width + "H: " + request.area.Height;
                response.dmCode = result.Text;
                recognizedFlag = true;
                presenter.showValidationError("");

            }
            else if (result == null)
            {
                Console.WriteLine("decode, failed");
                presenter.showValidationError("no Code detected");
                return;

            }


            Console.WriteLine(response.rectangleDimension);
            Console.WriteLine(response.dmCode);
            presenter.present(response);

            //decodeArea.Dispose();   
            return;

        }
    }
}
