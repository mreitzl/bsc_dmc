﻿using RealtimeDataMatriceDecoding.core.response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.boundaries
{
    public interface IDecodePresenter
    {
        void present(DecodeResponse response);
        void showValidationError(string errMsg);
    }
}
