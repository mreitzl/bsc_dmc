﻿using RealtimeDataMatriceDecoding.core.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.boundaries
{
    public interface IDecodeView
    {
        void show(DecodeViewModel viewModel);
        void showValidationError(string errorMessage);
    }
}
