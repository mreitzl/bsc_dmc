﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace RealtimeDataMatriceDecoding.core.entities
{
    public class FileLoader
    {

        public bool isloaded = false;
        OpenFileDialog openFileDialog1 = new OpenFileDialog();


        Bitmap loadedImage;
        public Bitmap LoadImage()
        {

            openFileDialog1.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            openFileDialog1.Title = "Browse Image";
            openFileDialog1.RestoreDirectory = true;
            //openFileDialog1.ShowDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                loadedImage = new Bitmap(openFileDialog1.FileName);
            }


            //openFileDialog1.Dispose();

            return loadedImage;
        }


    }
}
