﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.entities
{
    public class AdjustableRectangle
    {
        public Pen pen = new Pen(Color.Red);
        public bool isDrawn = false;
        //public int rectW;// = 131;
        //public int rectH;// = 63;
        //public int rectX;// = 145;
        //public int rectY;// = 119;
        public int rectW = 131;
        public int rectH = 63;
        public int rectX = 155;
        public int rectY = 119;
    }
}
