﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.entities
{
    class Converter
    {
        public Bitmap GrayScaleFilter(Bitmap image)
        {
            Bitmap grayScale = new Bitmap(image.Width, image.Height);
            for (int y = 0; y < grayScale.Height; y++)
            {
                for (int x = 0; x < grayScale.Width; x++)
                {
                    Color pixelColor = image.GetPixel(x, y);
                    Color setColor = Color.FromArgb(pixelColor.R, 0, 0);
                    image.SetPixel(x, y, setColor);

                }
            }
            return grayScale;
        }

        public Bitmap ConvertToGray(Bitmap image)
        {
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    Color c = image.GetPixel(x, y);

                    int r = c.R;
                    int g = c.G;
                    int b = c.B;
                    int avg = (r + g + b) / 3;
                    image.SetPixel(x, y, Color.FromArgb(avg, avg, avg));
                }
            }
            return image;
        }


        public Bitmap ConvertToBlackWhite(Bitmap image)
        {

            //Bitmap output = new Bitmap(cropIMG.Width, cropIMG.Height);

            //for (int i = 0; i < image.Width; i++)
            //{
            //    for (int j = 0; j < image.Height; j++)
            //    {
            //        Color c = image.GetPixel(i, j);

            //        int average = ((c.R + c.B + c.G) / 3);
            //        //Console.WriteLine(average);
            //        if (average < 8)
            //        {
            //            image.SetPixel(i, j, Color.Black);
            //        }  
            //        else
            //        {
            //            image.SetPixel(i, j, Color.White);
            //        }
            //    }
            //}

            return image;

        }
    }
}
