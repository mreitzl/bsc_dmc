﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;
using ZXing.Common;

namespace RealtimeDataMatriceDecoding.core.entities
{
    public class _Decoder
    {
        public BarcodeReader reader = new BarcodeReader
        {

            AutoRotate = true,
            TryInverted = true,
            Options = new DecodingOptions()
            {
                TryHarder = true,
                //ReturnCodabarStartEnd = true,
                PureBarcode = false,
                PossibleFormats = new List<BarcodeFormat>() {
                                //BarcodeFormat.QR_CODE,
                                //BarcodeFormat.UPC_A,
                                //BarcodeFormat.UPC_E,
                                //BarcodeFormat.EAN_8,
                                //BarcodeFormat.EAN_13,
                                //BarcodeFormat.CODE_39,
                                //BarcodeFormat.CODE_93,
                                //BarcodeFormat.CODE_128,
                                //BarcodeFormat.CODABAR,
                                //BarcodeFormat.ITF,
                                //BarcodeFormat.RSS_14,
                                //BarcodeFormat.PDF_417,
                                //BarcodeFormat.RSS_EXPANDED,
                                BarcodeFormat.DATA_MATRIX }
            }
        };

    }
}
