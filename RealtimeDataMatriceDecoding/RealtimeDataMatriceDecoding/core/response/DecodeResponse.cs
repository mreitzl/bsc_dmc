﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealtimeDataMatriceDecoding.core.response
{
    public class DecodeResponse
    {
        public string imageName;
        public string rectangleDimension;
        public string dmCode;
    }
}
